import { defineConfig } from "vitepress";

const gitlabIcon =
  '<svg clip-rule="evenodd" fill-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2" version="1.1" viewBox="0 0 193 185" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><g transform="translate(-93.988 -97.52)"><path d="m282.83 170.73-0.27-0.69-26.14-68.22c-0.532-1.337-1.474-2.471-2.69-3.24-2.487-1.544-5.692-1.372-8 0.43-1.117 0.907-1.927 2.136-2.32 3.52l-17.65 54h-71.47l-17.65-54c-0.383-1.391-1.195-2.627-2.32-3.53-2.308-1.802-5.513-1.974-8-0.43-1.214 0.772-2.155 1.905-2.69 3.24l-26.19 68.19-0.26 0.69c-7.708 20.139-1.115 43.113 16.1 56.1l0.09 0.07 0.24 0.17 39.82 29.82 19.7 14.91 12 9.06c2.876 2.184 6.884 2.184 9.76 0l12-9.06 19.7-14.91 40.06-30 0.1-0.08c17.175-12.988 23.755-35.921 16.08-56.04z" /></g></svg>';

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Alexander Müdespacher",
  description:
    "Hello, I'am Alexander. A web developer from Zurich, Switzerland.",
  head: [
    [
      "link",
      {
        rel: "icon",
        href: "data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>👨‍💻</text></svg>",
      },
    ],
  ],
  themeConfig: {
    siteTitle: "Alexander Müdespacher",
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      // { text: 'Home', link: '/' },
      { text: "👋🏻 About", link: "/about" },
      { text: "🛠️ Projects", link: "/projects" },
      { text: "📚 Articles", link: "/articles" },
      { text: "🗺️ Travels", link: "/travels" },
      { text: "🫱🏻‍🫲🏼 Get in touch", link: "/contact" },
    ],
    socialLinks: [
      {
        icon: "linkedin",
        link: "https://www.linkedin.com/in/alexandermuedespacher/",
      },
      { icon: "threads", link: "https://threads.net/@amuedespacher" },
      {
        icon: { svg: gitlabIcon },
        link: "https://gitlab.com/alexander.muedespacher",
      },
    ],
    footer: {
      copyright: "Copyright © 2023-present Alexander Müdespacher",
    },
    externalLinkIcon: true,
  },
});
