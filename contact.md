# 🫱🏻‍🫲🏼 Contact {#contact}

Feel free to get in touch with me

<form name="contact" method="POST" data-netlify="true" id="contactForm">
    <label>Your Name:<br><input type="text" name="name" required /></label>
    <label>Your Email:<br><input type="email" name="email" required /></label>
    <label>Message:<br><textarea name="message" required></textarea></label>
    <button type="submit">🚀 Send message</button>
</form>
