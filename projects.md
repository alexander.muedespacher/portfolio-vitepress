# 🛠 Projects {#projects}

Welcome to my digital playground! From first steps with HTML to building web applications from A to Z, here's a tour through the projects that keep me up at night (in a good way, I promise).

## 💡 fairQ.app - Web platform for managing waiting lists {#fairq}

### Motivation

Tired of intransparent and unfair waiting lists? Me too. That's why we started building **fairQ** as a capstone project at the Fullstack Web Development bootcamp at [Constructor Acacdemy](https://academy.constructor.org/en) aiming to streamline waiting lists for organizations and applicants alike.

### Technology Stack

- **Backend**: Django / Django Rest Framework
- **Frontend**: React / Tailwind
- **Database**: Postgres
- **DevOps**: Docker Compose / GitLab CI/CD

### What I Learnt

Implementing a full-scale project from scratch taught me the nuances of Django and how to integrate a React frontend seamlessly.

### Links

Visit [fairq.app](https://fairq.app) for more, or have a read on Constructor's [blog post](https://academy.constructor.org/blog/full-stack-capstone-projects-batch-24).

## 🏔️ 46north.ch - Outdoor blog {#46north}

### Motivation

My love for the outdoors needed an outlet. **46north.ch** is where I document my hiking and skiing adventures, complete with stunning visuals and trail guides.

### Technology Stack

- **Frontend**: Next.js / Tailwind / DaisyUI --> want to migrate to [Astro](https://astro.build)
- **CMS**: Contentful --> want to migrate to [Strapi CMS](https://strapi.io)
- **Hosting**: Vercel

### What I Learnt

How to integrate a headless CMS with a Next.js frontend and making it look good with Tailwind and DaisyUI.

### Links

Find your next hike on [46north.ch](https://46north.ch). Or check out the git repo [here](https://gitlab.com/alexander.muedespacher/46north-nextjs)

## 👨‍💻 Older Pet Projects {#pet-projects}

### nearby

A simple tool to find the nearest public transport stop and the next train/tram/bus connection in Switzerland.

- **Motivation:** Ever been stranded looking for the next bus home? That's why I built **nearby**.
- **Technology Stack:** PHP
- **What I learnt:** Basic PHP scripting and consuming a REST API
- **How It's Used Today:** Though it's an older project, it's still functional and practical for those on-the-go.
- **Link**: https://nearby.alexandermuedespacher.ch/

## 💼 Freelance Projects {#freelance}

### brennholz-bachtel.ch - Brennholz Bachtel

Website for a local firewood business and farm products run by my family.

- **Motivation:** My family needed a digital storefront, and who better to build it than their tech-savvy son? In the mean time, my brother took over the family business.
- **Technology Stack:** WordPress + WooCommerce
- **What I learnt:** Basics of e-commerce and WordPress customization.
- **How It's Used Today:** Actively helping my brother manage sales and showcase farm products (firewood and meat).
- **Link**: https://brennholz-bachtel.ch/

### fgv-zo.ch - Familiengartenverein Zürich Ost

Web platform for the Familiengartenverein Zürich Ost, the association where I rent a garden plot.

- **Motivation:** I have a garden here, and the association needed somebody to take over the digital presence.
- **Technology Stack:** WordPress
- **How It's Used Today:** Actively used by current and future garden enthusiasts to stay updated and engaged.
- **Link**: https://fgv-zo.ch/

### barberniederdorf.ch - Barber Niederdorf

A sleek website for my favorite barber in Zurich, Ibrahim.

- **Motivation:** Ibrahim is great with scissors but needed a hand online.
- **Technology Stack:** Static Site / GitLab Pages
- **What I learnt:** How to integrate Google MyBusiness API for real-time data like opening hours.
- **How It's Used Today:** Actively helping Ibrahim snip his way to success.
- **Link**: https://barberniederdorf.ch/

---

Feel like talking? Let's collaborate! Head over to the [contact page](/contact) to get in touch.
