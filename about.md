# 👋🏻 About me {#alex}

<img src="/assets/alex.webp" class="avatar" alt="Alexander Müdespacher" />

## 👨‍💻 Who Am I? {#me}

Hi, I'm Alex — a Fullstack Developer rooted in the heart of Switzerland, Zürich. When I'm not busy building digital products, you'll find me conquering mountains or tending to my garden plot.

## ⚙️ What I Do {#job}

By day, I'm mapping out the virtual world for the [Atlas der Schweiz](https://atlasderschweiz.ch) at the [Institute of Cartography and Geoinformation](https://ikg.ethz.ch/en/) at [ETH Zürich](https://ethz.ch) and by evening, I'm shaping future devs as an instructor at [Constructor Academy](https://academy.constructor.org/de/full-stack/zurich)'s fullstack web development bootcamp.

## 🏆 My journey so far {#career}

I started by learning how big companies work at [Swiss Life](https://swisslife.com). Then, I moved on to digital consulting and managing digital projects at Namics AG (today: [Merkle](https://www.merkle.com/)) and [Open Web Technology](https://www.owt.swiss/en/). After that, I led product and tech teams at [VIU Eyewear](https://shopviu.com), a retail startup in the eyewear industry, and [Clyde Mobility](https://clyde.ch), a corporate startup in mobility subscription.

But I always wanted to code, not just manage teams of developers. So, I switched gears and became a full-stack web developer.

## 🎓 Education {#education}

- Full-Stack Web Development Bootcamp covering HTML, CSS, JavaScript, React, Python, Django, and DevOps at **Constructor Learning, Zürich**.
- Master of Arts in Business Administration and a degree in Business Education at **University of St. Gallen (HSG)**.
- Bachelor of Science in Business Administration with a focus on Business Informatics at **Zürcher Hochschule für Angewandte Wissenschaften (ZHAW)**.

## 🛠 Toolbox {#tools}

Over the years, I've gotten my hands dirty with a variety of languages and tools. Here's the lineup I'm most comfy with:

### Frontend

- **React / Next.js**: The first JS framework I learned.
- **Vue.js**: My go-to for new projects today.
- **Tailwind CSS**: My prferred CSS framework.
- **Astro**: My new favorite static site generator.
- **Deck.gl**: For all things data visualization on maps.

### Backend

- **Python / Django**
- **Node.js / Express**

### DevOps

- **Docker**
- **Gitlab CI/CD**

Want to see a few of my projects? Head on over to the [Projects page](/projects).

## 🌍 Beyond the code {#leisure}

🏔 **Outdoor Enthusiast**: From hiking and ski touring to alpinism, I capture my mountain escapades on my blog [46north.ch](https://46north.ch).

🌱 **Green Thumb**: Gardening isn't just a hobby; it's my escape from the digital world. A perfect way to focus on something other than code.

🚲 **Gravel Grinder**: Exploring my local area by bike is a joy. Want to keep pace? Find me on [Strava](https://www.strava.com/athletes/3610216).

🌍 **Traveler**: I prefer the road less traveled. Check out some of my most memorable journeys [here](/travels).

## 💪 Strengths {#strengths}

- I take my tasks seriously and see them through to completion.
- Good at forming and maintaining strong connections.
- Curiosity: Never stop learning, whether it's mastering a new tech stack or scaling a mountain.
- Trust in my own skills and judgement.

---

Got questions? Just slide into my [Contact page](/contact).
