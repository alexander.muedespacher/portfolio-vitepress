---
title: Articles
---

# Articles

Here are some notes, tutorials, and articles around software development. I hope you find them useful.

## Wordpress

- [Migrating a Wordpress blog to Astro]() - Coming soon

## Deck.gl / Data Visualization

- [Creating a 'locate me' layer in Deck.gl]() - Coming soon

## DevOps

- [Setting up a Django application with Docker and Gitlab CI/CD](./setting-up-a-django-application-with-docker-and-gitlab-ci-cd.md)
