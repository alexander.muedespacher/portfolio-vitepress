---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Hi, I'm Alexander."
  text: "A web developer from Zurich, Switzerland 🇨🇭"
  actions:
    - theme: brand
      text: 👋🏻 About me
      link: /about
    - theme: alt
      text: 🛠️ Projects
      link: /projects
    - theme: alt
      text: 🗺️ Travels
      link: /travels
features:
  - icon: 👨‍💻
    title: Coding Enthusiast
    details: Always exploring new technologies, building exciting projects, and teaching others how to code.
  - icon: 🎒
    title: Outdoor Lover
    details: Exploring the great outdoors, from hiking and mountaineering to travelling the world, to documenting adventures on my blog.
  - icon: 🧑‍🌾
    title: Green Thumb
    details: Nurturing plants and flowers. From veggies to flowers, the garden is my oasis of calm.
---
