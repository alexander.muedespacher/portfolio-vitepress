# Portfolio website with VitePress

My personal portfolio website built with Vitepress and hosted on Netlify.

More on Vitepress: https://vitepress.dev/

🔗 [Visit deployed page](https://alexandermuedespacher.ch)

🚀 Deployment status: [![Netlify Status](https://api.netlify.com/api/v1/badges/e05434e1-1e32-4042-9278-64e77c4119b6/deploy-status)](https://app.netlify.com/sites/amuedespacher/deploys)
