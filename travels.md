# 🗺️ Travels {#travels}

Beyond my regular adventures in the breathtaking Swiss Alps, I have a passion for venturing into remote corners of the globe, seeking out hidden gems and off-the-beaten-path experiences.

## 2022: Georgia - the third Time

<div class="big-emojis">🇬🇪🚗⛺️🌊🐶🍲</div>

Third time's a charm in Georgia! Hitting the road in a Lada Niva equipped with a roof tent, we went full nomad. From the mystical vibes of Svaneti to a quick dip in the Black Sea, some mouthwatering Georgian cuisine, and new furry friends found in the middle of nowhere. Checked out Vashlovani's barren beauty and took a final bow in ever-vibrant Tbilisi.

## 2019: Packrafting and Hiking in Kamchatka, Eastern Siberia

<div class="big-emojis">🇷🇺🌋🐻🛶🏕️</div>

Most people know Kamchatka from the board game Risk, but we heard it's a haven for hiking and packrafting too. Flying into Petropavlovsk, we got ourselves dropped in the wilderness near Gorely volcano with rented packrafts from Switzerland. The weather threw us a curveball, so our planned journey along the Opala River was a no-go. Instead, we explored other wild corners of this rugged peninsula. Bears? Check. Salmon? Double check. Mosquitoes? You can count on it. An unforgettable adventure in a place not easily revisited.

<div style="position: relative; overflow: hidden; width: 100%; padding-top: 56.25%;">
  <iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://www.youtube.com/embed/uZzmmUt7M9w" frameborder="0" allowfullscreen></iframe>
</div>

## 2018: Georgia - again

<div class="big-emojis">🇬🇪🥾🍇🌵</div>

Second rendezvous with Georgia since 2014, and it not disappoint. Roamed Tbilisi's cobblestone streets again and trekked to the Black lake in Lagodekhi, just a stone's throw from Dagestan. A dash through Vashlovani National Park and a sip in vineyards kept things sweet.

## 2016: Iceland

<div class="big-emojis">🇮🇸🚗❄️🌞</div>

Flew to Iceland for a week in late September. Zipped along the ring road in surprisingly good weather. Cold? Yep. Worth it? Absolutely. It's like if Mother Nature had a greatest hits album, this would be it. Saw some northern lights as well. Can't wait for the sequel.

## 2015: Pamir Highway by Bicycle

<div class="big-emojis">🇹🇯🇰🇬🚴🏻🚴🏻🛖🏕️</div>

Captivated by Central Asia, my brother Esra and I tackled the Pamir Highway on bicycles. Pedaling through Tajikistan and Kyrgyzstan on the world's second-highest international road, we faced 40°C heat in Dushanbe, scaled mountain passes, zoomed downhill, bunked by the Afghan border, mingled (and vodka'd) with locals, sweated dust, fixed flats, and savored the genuine warmth of people living simple lives. One month, two wheels, endless stories.

<div style="position: relative; overflow: hidden; width: 100%; padding-top: 56.25%;">
  <iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://www.youtube.com/embed/OlYAiah-930" frameborder="0" allowfullscreen></iframe>
</div>

## 2014: Mount Elbrus, Europe's highest Mountain

<div class="big-emojis">🇷🇺🏔️🎒⛺️</div>

Teamed up with my brother Esra, we jetted to Russia to scale Mount Elbrus, Europe's tallest peak. Taking the historic North route to the summit, we topped off the adventure with a few days exploring vibrant Moscow.

<div style="position: relative; overflow: hidden; width: 100%; padding-top: 56.25%;">
  <iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://www.youtube.com/embed/F1OjpwGetGI" frameborder="0" allowfullscreen></iframe>
</div>

## 2014: Mount Kazbek (Georgia)

<div class="big-emojis"> 🇬🇪🥾🎒🏔️</div>

Just before tackling Elbrus, my buddy Andrej and I summitted Mount Kazbek, straddling the Georgia-Russia border. High fives at 5,000 meters!

<div style="position: relative; overflow: hidden; width: 100%; padding-top: 56.25%;">
  <iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://www.youtube.com/embed/kCivnnvHyLU" frameborder="0" allowfullscreen></iframe>
</div>

## 2012: Kyrgyzstan to Switzerland with a Lada 2101

<div class="big-emojis">🇰🇬🇺🇿🇹🇲🇦🇿🇮🇷🇹🇷🇬🇷🇦🇱🇲🇪🇧🇦🇭🇷🇸🇮🇮🇹🇨🇭🚗</div>

Hopping into a 1984 Lada Shiguli, my brothers and I launched a cross-continental odyssey for the ages. Starting in Kyrgyzstan, we ventured into Uzbekistan, digging its ancient cities, then rolled into Turkmenistan to gawk at Ashgabat's glitz. Next, we freighter-hopped across the Caspian to Azerbaijan, then swung through Iran to climb its highest peak. From there, it was on to Turkey for some quality tea-time, followed by a zigzag through the Balkans—Albania, Montenegro, Bosnia, Croatia, and Slovenia—before winding down in Italy and finally reaching Switzerland. "Epic" barely scratches the surface.

🔗 Read the blog on [shiguli.ch](https://www.shiguli.ch)

<div style="position: relative; overflow: hidden; width: 100%; padding-top: 56.25%;">
  <iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://www.youtube.com/embed/maGjmndX0dY" frameborder="0" allowfullscreen></iframe>
</div>
